from flask import Flask, request, send_file, Response
from flask_cors import CORS
from functools import wraps
import cv2
import numpy as np

import requests

from service_registry import ServiceRegistry

app = Flask(__name__)


@app.route('/<service_name>/<path:path>', methods=['GET', 'POST', 'PUT', 'DELETE'])
def proxy_request(service_name, path):
    # get all
    # get_data = request.args.to_dict()

    # post all
    # post_data = request.form

    headers = request.headers

    # token特殊处理时使用
    # token = request.args.get('token')
    # print(token)
    # headers = {"Content-Type":"application/json"}
    # if token is None:
    #     print("No token")
    # else:
    #     print("Token found:" + str(token))
    #     headers["Authorization"] = token

    service_url = registry.discover_service(service_name)
    if service_url:
        if service_name == 'stream':
            url = f"http://{service_url}/{path}"
            return Response(gen_frames(url, request.method, request.data, headers), mimetype='multipart/x-mixed-replace; boundary=frame')
        else:
            url = f"http://{service_url}/{path}"
            response = requests.request(request.method, url, data=request.data, headers=headers, params=request.args.to_dict())
            contentType = response.headers['Content-Type']
            if contentType == 'text/html; charset=utf-8':
                return response.text, response.status_code
            elif contentType == 'image/jpeg':
                with open('image.jpg', 'wb') as f:
                    f.write(response.content)
                return send_file('image.jpg')
            else:
                return response.text, response.status_code
    else:
        return "Service not found", 404


@app.route('/<service_name>', methods=['GET', 'POST'])
def proxy_request_main(service_name):
    service_url = registry.discover_service(service_name)
    if service_url:
        url = f"http://{service_url}"
        response = requests.request(request.method, url, data=request.data)
        return response.text, response.status_code
    else:
        return "Service not found", 404


def gen_frames(url, m, d, h):
    response = requests.request(m, url, data=d, headers=h, stream=True)
    if response.status_code == 200:
        empty_bytes = bytes()
        for chunk in response.iter_content():
            empty_bytes += chunk
            a = empty_bytes.find(b'\xff\xd8')
            b = empty_bytes.find(b'\xff\xd9')
            if a != -1 and b != -1:
                jpg = empty_bytes[a:b + 2]
                empty_bytes = empty_bytes[b + 2:]
                with open('image.jpg', 'wb') as f:
                    f.write(jpg)
                image = cv2.imread('image.jpg')  # 读取图像
                _, buffer = cv2.imencode('.jpg', image)
                frame = buffer.tobytes()
                yield (b'--frame\r\n'
                       b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # 发送视频帧


CORS(app, resources=r'/*')
if __name__ == '__main__':
    # 注册中心
    registry = ServiceRegistry(host='localhost', port=2379)

    # 示例
    registry.register_service('service1', '127.0.0.1', 5000)
    registry.register_service('service2', '127.0.0.1', 5001)

    # 前端项目
    registry.register_service('backend', '127.0.0.1', 8000)

    # 图片、视频流
    registry.register_service('img', '127.0.0.1', 4051)
    registry.register_service('stream', '127.0.0.1', 4052)

    app.run(debug=True, port=8050)
