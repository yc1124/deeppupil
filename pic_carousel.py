from flask import Flask, send_file, Response
import cv2
import time
import random

app = Flask(__name__)


@app.route('/image')
def get_image():
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')

    # return send_file('static/images/pic1.jpg')


def gen_frames():
    while True:
        time.sleep(2)
        random_number = random.randint(1, 7)
        pic_path = 'static/images/pic' + str(random_number) + '.jpg'
        print(pic_path)
        image = cv2.imread(pic_path)  # 读取图像
        _, buffer = cv2.imencode('.jpg', image)
        frame = buffer.tobytes()
        yield (b'--frame\r\n'
                b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')  # 发送视频帧


if __name__ == '__main__':
    app.run(debug=True, port=4052)
