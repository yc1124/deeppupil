import requests

API_HARDWARE_URL = 'http://localhost:8050/backend/api/configuration/hardware'

# 更新硬件配置的连接状态接口
def change_connect_status():
    # 更新硬件配置的连接状态接口的URL
    url = API_HARDWARE_URL + '/change_connect_status'
    # 参数：硬件编号，目前硬件编号暂时在数据库中写死
    url += '?code=' + 'HN00006'
    # 参数：状态，0：未连接，1：已连接
    url += '&status=' + '0'
    # 发送GET请求
    response = requests.get(url)
    # 检查响应状态
    if response.status_code == 200:
        # 获取响应内容（假设是JSON格式）
        data = response.json()
        print(data)
    else:
        print('Failed to retrieve data: ', response.status_code)


# 更新硬件配置的使用时长接口
def change_duration():
    # 该接口调用一次使用时长加1小时
    url = API_HARDWARE_URL + '/change_duration'
    # 参数：硬件编号，目前硬件编号暂时在数据库中写死
    url += '?code=' + 'HN00006'
    # 发送GET请求
    response = requests.get(url)
    # 检查响应状态
    if response.status_code == 200:
        # 获取响应内容（假设是JSON格式）
        data = response.json()
        print(data)
    else:
        print('Failed to retrieve data: ', response.status_code)


if __name__ == '__main__':
    change_connect_status()
    # change_duration()
